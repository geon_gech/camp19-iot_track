#define LED 

void setup() {
  pinMode(LED, OUTPUT);     
}

void loop() {
  digitalWrite(LED, LOW);   
  delay(500);                      
  digitalWrite(LED, HIGH); 
  delay(500);                      
}
